var Templates = {
    _escapeToHtml: function(string) {
        return $('<p/>').text(string).html();
    },
    User: function(details, vote) {
        return $('<div/>')
            .append($('<span/>').html(this._escapeToHtml(details.displayName + (details.email ? " " + details.email : ""))))
            .append($('<span/>').addClass('vote').html(this._escapeToHtml(vote ? vote : "")))
    }
}