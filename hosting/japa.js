// Initialize Firebase
var config = {
    apiKey: "AIzaSyBbxcHsg-ZeNPHm0G1ItpHLh7X2VKdqasQ",
    authDomain: "japa-72729.firebaseapp.com",
    databaseURL: "https://japa-72729.firebaseio.com",
    storageBucket: "japa-72729.appspot.com",
};
firebase.initializeApp(config);

//debug:
firebase.dump = function(ref) {
    firebase.database().ref(ref).on('value', function(snapshot) {
        console.log(snapshot.val());
    });
}

//not-so-debug:
firebase.onChange = function(ref, fn) {
    firebase.database().ref(ref).on('value', function onChange(snapshot) {
        fn(snapshot.val());
    });
}

firebase.set = function(ref, value) {
    firebase.database().ref(ref).set(value);
}
firebase.update = function(ref, value) {
    firebase.database().ref(ref).update(value);
}
firebase.remove = function(ref) {
    firebase.database().ref(ref).remove();
}

$(function() {
    var registerPresence = function() {
        var uid = firebase.auth().currentUser.uid;

        var userRef = firebase.database().ref('users/' + uid);
        var connections = userRef.child('connections');
        var connection = connections.push(true);
        connection.onDisconnect().remove();

        Cookies.set('firebase-uid', uid);

        userRef.on('value', function(snapshot) {
            var value = snapshot.val();
            if (!value.details) {
                $('.login-form-container').show();
            } else {
                $('.login-form-container').hide();
                $('.users-container').show();
                $('.your-vote-container').show();
            }

            if (value.vote) {
                $('.your-vote-container').find('input[name="your-vote"]').val(value.vote);
            }
        })
    }

    firebase.onChange('.info/connected', function(value) {
        if (value === true) {
            registerPresence();

        }
    });

    var setCurrentUserDetails = function(details) {
        firebase.update('users/' + firebase.auth().currentUser.uid + '/details', details);
    };

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
//            var uid = user.uid;
//            registerPresence(uid);
        } else {
            Cookies.remove('firebase-uid');
        }
    });

    $('.login-form-container').find('form').on('submit', function (e) {
        e.preventDefault();
        var displayName = $('input[name="display-name"]', this).val();
        var email = $('input[name="email"]', this).val();
        if (!displayName.trim()) {
            $('input[name="display-name"]', this).parent().find('.field-error').show();
        } else {
//            $('.login-form-container').hide();
            setCurrentUserDetails({
                displayName: displayName.trim(),
                email: email.trim()
            })
        }
    });

    $('.your-vote-container').find('form').on('submit', function (e) {
        e.preventDefault();
        var vote = $('input[name="your-vote"]', this).val();
        firebase.set('users/' + firebase.auth().currentUser.uid + '/vote', vote);
    });


    var previousUid = Cookies.get('firebase-uid');
    if (!previousUid) {
        firebase.auth().signInAnonymously();
    } else {
//        registerPresence(previousUid);
    }

    firebase.onChange('users', function(users) {
        var $container = $('.users-container');
        $container.empty();
        $.each(users, function(uid, user) {
            if (user.details && user.connections) {
                $container.append(Templates.User(user.details, user.vote));
            }
        });
        console.log(users);
    });
});